<?php
namespace mywishlist\Models ;

    class Cagnotte extends \Illuminate\Database\Eloquent\Model
    {
        protected $table = 'cagnotte';
        protected $primaryKey = 'no';
        public $timestamps = false;

        static public function getCagnotte($id){
            $cs = Cagnotte::get();
            $p = 0;
            foreach ($cs as $c){
                if($c->noItem == $id)
                    $p = $p + $c->valeur;
            }
            return $p;
        }

        static public function getParticipant($id){
            $cs = Cagnotte::get();
            $p[] = null;
            foreach ($cs as $c){
                if($c->noItem == $id) {
                    if(!in_array($c->participant,$p))
                        $p[] = $c->participant;
                }
            }
            return $p;
        }
    }