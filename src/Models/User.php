<?php
/**
 * Created by PhpStorm.
 * User: PoweR
 * Date: 20/12/2017
 * Time: 16:49
 */

namespace mywishlist\Models;


class User extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'user';
    protected $primaryKey = 'login';
    public $timestamps = false;

    static function createUser($login, $nom, $prenom, $mdp, $mail)
    {
        $tmp = new User();
        $tmp->login = $login;
        $tmp->nom = $nom;
        $tmp->prenom = $prenom;
        $tmp->password = password_hash($mdp, PASSWORD_DEFAULT);
        $tmp->email = $mail;

        $tmp->save();
    }

    static function getNom($id){
        $u = User::where('id','=',$id)->first();
        return $u->nom;
    }
}