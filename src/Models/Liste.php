<?php
namespace mywishlist\Models ;
/**
 * Created by PhpStorm.
 * User: PoweR
 * Date: 21/11/2017
 * Time: 10:21
 */
class Liste extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'liste';
    protected $primaryKey = 'no';
    public $timestamps = false;

    public static function getTest($i, $p) {
        $date = Date('Y-m-d');
        $listes = Liste::get();
        $arr = null;
        foreach ($listes as $liste){
            if($liste->user_id == $i)
                if ($p == 0) {
                    if ($liste->expiration < $date)
                        $arr[] = $liste;
                }else{
                    if ($liste->expiration >= $date)
                        $arr[] = $liste;
                }

        }
        return $arr;
    }

    public static function peutsuppr($no){
        $items = Liste::getTest();

        foreach ($items as $item) {
            if ($item->réservation != null)
                $b = 0;
            if ($item->cagnotte == 'on' and Cagnotte::getCagnotte($item->id) != 0)
                $b = 0;
        }
    }

    public static function getProprio($i){
        $liste = Liste::get();
        $res = null;
        foreach ($liste as $l){
            if($l->no == $i)
                $res = $l->user_id;
        }
        return $res;
    }

    public static function getNo($i){
        $liste = Liste::get();
        $res = null;
        foreach ($liste as $l){
            if($l->token == $i)
                $res = $l->no;
        }
        return $res;
    }

    public static function getToken($i){
        $liste = Liste::get();
        $arr = null;
        foreach ($liste as $l){
            if($l->no == $i)
                $arr = $l->token;
        }
        return $arr;
    }



    public static function createListe(){
        $app = \Slim\Slim::getInstance();
        $n = $app->request->post('nom');
        $d = $app->request->post('descr');
        $date = $app->request->post('date');
        $token = random_bytes(32);
        $token = bin2hex($token);

        $l = new liste();
        $l->titre = $n;
        $l->description = $d;
        $l->token = $token;
        $l->expiration = $date;
        $l->user_id = $_SESSION['id'];
        $l->public = 0;

        $l->save();
    }
}
