<?php

namespace mywishlist\Controller;

use mywishlist\Models\Liste;
use mywishlist\View\CagnotteView;
use mywishlist\View\ConnexionView;
use mywishlist\View\CreatorView;
use mywishlist\View\HomeView;
use mywishlist\View\InscriptionViewer;
use mywishlist\View\ItemView;
use mywishlist\View\ItemListView;
use mywishlist\View\listepublicView;
use mywishlist\View\ListView;
use mywishlist\View\ModifInfoView;
use mywishlist\View\ModifItemView;
use mywishlist\View\ModifPassView;
use mywishlist\View\MoncompteView;
use mywishlist\View\NewListView;
use mywishlist\View\NewItemView;
use mywishlist\View\ReservationItem;
use mywishlist\View\SuppressionView;
use mywishlist\View\ModifListeView;


class Controller
{
    static function showConnection()
    {
        $v = new ConnexionView();
        echo $v->render();
    }

    static function showHome()
    {
        $v = new HomeView();
        echo $v->render();
    }

    static function showInscription()
    {
        $v = new InscriptionViewer();
        echo $v->render();
    }

    static function showItem($id)
    {
        $_SESSION['item'] = $id;
        $v = new ItemView($id);
        echo $v->render();
    }

    static function showItemList($num)
    {
        $_SESSION['liste'] = Liste::getNo($num);
        $v = new ItemListView($num);
        echo $v->render();
    }

    static function showList()
    {
        $v = new ListView();
        echo $v->render();
    }

    static function showModifInfo()
    {
        $v = new ModifInfoView();
        echo $v->render();
    }

    static function showModifPass()
    {
        $v = new ModifPassView();
        echo $v->render();
    }

    static function showMonCompte()
    {
        $v = new MoncompteView();
        echo $v->render();
    }

    static function showNewItem()
    {
        $v = new NewItemView();
        echo $v->render();
    }

    static function showNewList()
    {
        $v = new NewListView();
        echo $v->render();
    }

    static function showSuppression(){
        $v = new SuppressionView();
        echo $v->render();
    }

    static function showModifListe(){
        $v = new ModifListeView();
        echo $v->render();
    }

    static function showModifItem(){
        $v = new ModifItemView();
        echo $v->render();
    }

    static function showReservItem($id){
        $v = new ReservationItem();
        echo $v->render($id);
    }

    static function showListepublic(){
        $v = new listepublicView();
        echo $v->render();
    }

    static function showCagnotte($id){
        $v = new CagnotteView($id);
        echo $v->render();
    }

    static function showCreator(){
        $v = new CreatorView();
        echo $v->render();
    }
}