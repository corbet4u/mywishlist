<?php
namespace mywishlist\Controller;

use mywishlist\Models\Cagnotte;
use mywishlist\Models\User;
use mywishlist\Models\Liste;
use mywishlist\Models\Item;

class Connect
{
    static function connection(){
        $app =\Slim\Slim::getInstance();
        $login = filter_var($app->request->post('login'),FILTER_SANITIZE_STRING);
        $password = filter_var($app->request->post('password'),FILTER_SANITIZE_STRING);

        $a = User::where('login','=',$login)->first();
        if($a==null) $app->redirect($app->urlFor('connexion'));
        if (password_verify($password, $a->password)) {
            $_SESSION['id'] = $a->id;
            setcookie("proprio", $a->id, time()+31536000);
            $app->redirect($app->urlFor('route_home'));
        }
        else
            $app->redirect($app->urlFor('connexion'),['e'=>true]);
    }

    static function disconnect(){
        if(isset($_SESSION['id']))
            unset($_SESSION['id']);
        $app =\Slim\Slim::getInstance();
        $app->redirect($app->urlFor('route_home'));
    }

    static function newliste(){

        $nom = filter_var($_POST['nom'],FILTER_SANITIZE_STRING);
        $descr = filter_var($_POST['descr'],FILTER_SANITIZE_STRING);


        Liste::createListe();

        $app =\Slim\Slim::getInstance();
        $app->redirect($app->urlFor('route_liste'));
    }

    static function newitem(){

        $app =\Slim\Slim::getInstance();
        $nom = filter_var($_POST['nom'],FILTER_SANITIZE_STRING);
        $descr = filter_var($_POST['descr'],FILTER_SANITIZE_STRING);
        $tarif =  filter_var($app->request->post('tarif'),FILTER_SANITIZE_STRING);
        $img = filter_var( $app->request->post('image'),FILTER_SANITIZE_STRING);
        $vente =  filter_var($app->request->post('vente'),FILTER_SANITIZE_STRING);

        $cagnotte = filter_var($app->request->post('cagnotte'), FILTER_SANITIZE_STRING);


        Item::createItem($nom,$descr,$tarif,$img,$vente,$cagnotte);

        $app =\Slim\Slim::getInstance();
        $app->redirect($app->urlFor('route_itemliste',['token'=>Liste::getToken($_SESSION['liste'])]));
    }

    static function inscrire(){

        $app =\Slim\Slim::getInstance();
        $login = filter_var($app->request->post('login'),FILTER_SANITIZE_STRING);
        $prenom = filter_var($app->request->post('prenom'),FILTER_SANITIZE_STRING);
        $nom = filter_var($app->request->post('nom'),FILTER_SANITIZE_STRING);
        $email = filter_var($app->request->post('email'),FILTER_SANITIZE_EMAIL);
        $password = filter_var($app->request->post('password'),FILTER_SANITIZE_STRING);

        User::createUser($login,$nom,$prenom,$password,$email);

        $app->redirect($app->urlFor('route_home'));
    }

    static function supprliste($fin = false){

        $l = Liste::where('no','=',$_SESSION['liste'])->first();
        $items = Item::getTest($_SESSION['liste']);
        $b = 1;
        if($items!=null){
            foreach ($items as $item) {
                if ($item->reservation != null)
                    $b = 0;
                if ($item->cagnotte == 'on' and Cagnotte::getCagnotte($item->id) != 0)
                    $b = 0;
            }
        }
        if($b == 1) {
            if($items!=null)
                foreach ($items as $item)
                    $item->delete();
            $l->delete();
        }

        $app =\Slim\Slim::getInstance();
        if($fin == false)
            $app->redirect($app->urlFor('route_liste'));

    }

    static function getHeader()
    {
        $app = \Slim\Slim::getInstance();
        $root = $app->request->getRootUri();
        $html = '
        <!DOCTYPE html>
        <html>
        <head>
            <!--Import Google Icon Font-->
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <!--Import materialize.css-->
            <link type="text/css" rel="stylesheet" href="'.$root.'/css/materialize.min.css"  media="screen,projection"/>
            <!--Let browser know website is optimized for mobile-->
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Liste des listes</title>
        </head>';

        return $html;
    }

    static function getDebutNav()
    {
        $app = \Slim\Slim::getInstance();
        $url = $app->urlFor('route_liste', []);
        $url2 = $app->urlFor('inscription', []);
        $url4 = $app->urlFor('connexion', ['e'=>false]);
        $url5 = $app->urlFor('deconnect', []);
        $url6 = $app->urlFor('moncompte', []);
        $url3 = $app->urlFor('route_home');
        $html = '<nav><div class="nav-wrapper"><a href="'.$url3.'" class="brand-logo">MyWishList</a><ul id="nav-mobile" class="right hide-on-med-and-down">';

        if (!isset($_SESSION['id'])) {
            $html = $html . '
            <li><a class="waves-effect waves-light btn" href=' . $url2 . '>Inscription</a></li>
            <li></li><a class="waves-effect waves-light btn" href=' . $url4 . '>Connexion</a></li>
            ';
        } else {
            $html = $html . '
             
            <li><a class="waves-effect waves-light btn" href=' . $url . '>Mes Listes</a></li>
            <li></li><a class="waves-effect waves-light btn" href=' . $url6 . '>Mon Compte</a></li>
            <li></li><a class="waves-effect waves-light btn" href=' . $url5 . '>Deconnexion</a></li>
            ';

        }

        return $html;
    }

    static function getFinNav()
    {
        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        return '</ul></div></nav>
        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="'.$url.'/js/materialize.min.js"></script>';
    }

    static function modifmdp(){

        $app =\Slim\Slim::getInstance();

        $password = filter_var($app->request->post('password'),FILTER_SANITIZE_STRING);
        $npassword = filter_var($app->request->post('npassword'),FILTER_SANITIZE_STRING);

        $user = User :: where('id','=',$_SESSION['id'])->first();
        if (password_verify($password, $user->password)) {
            $user->password = password_hash($npassword,PASSWORD_DEFAULT);
            $user->update();
            if(isset($_SESSION['id']))
                unset($_SESSION['id']);
            $app->redirect($app->urlFor('connexion'));

        }
        else{
            $app->redirect($app->urlFor('modificationPassword'));
        }
    }

    static function modifinfo(){
        $app =\Slim\Slim::getInstance();

        $nom = filter_var($app->request->post('nom'),FILTER_SANITIZE_STRING);
        $prenom = filter_var($app->request->post('prenom'),FILTER_SANITIZE_STRING);


        $mail = filter_var($app->request->post('email'),FILTER_SANITIZE_EMAIL);
        $user = User :: where('id','=',$_SESSION['id'])->first();


        $user->nom=$nom;
        $user->prenom=$prenom;
        $user->email=$mail;
        $user->update();
        $app->redirect($app->urlFor('moncompte'));
    }

    static function suppricompte(){
        $app =\Slim\Slim::getInstance();
        $user = User :: where('id','=',$_SESSION['id'])->first();
        $id =  Liste :: where('user_id','=',$user->id)->get();
        foreach ($id as $list){
            $_SESSION['liste'] = $list->no;
            Connect::supprliste(true);
        }
        unset($_SESSION['liste']);
        unset($_SESSION['item']);
        unset($_SESSION['id']);

        $user->delete();

        $app->redirect($app->urlFor('route_home'));
    }

    static function modifListe(){
        $app =\Slim\Slim::getInstance();
        $id =  Liste :: where('no','=',$_SESSION['liste'])->first();

        $id->titre = $_POST['titre'];
        $id->description = $_POST['description'];

        $id->update();

        $app->redirect($app->urlFor('route_itemliste',['token'=>Liste::getToken($_SESSION['liste'])]));
    }

    static function modifItem($id){
        $app =\Slim\Slim::getInstance();
        $it =  Item :: where('id','=',$id)->first();

        $it->nom = $app->request->post('titre');
        $it->descr = $app->request->post('description');
        $it->tarif = $app->request->post('tarif');
        $it->img = $app->request->post('image');
        $it->url = $app->request->post('vente');

        $it->update();

        $app->redirect($app->urlFor('route_itemliste',['token'=>Liste::getToken($_SESSION['liste'])]));
    }

    static function suppritem($id){
        $l = Item::where('id','=',$id);
        $l -> delete();

        $app =\Slim\Slim::getInstance();
        $app->redirect($app->urlFor('route_itemliste',['token'=>Liste::getToken($_SESSION['liste'])]));
    }

    static function reserverItem($id){
        $app = \Slim\Slim::getInstance();
        $l = Item::where('id','=',$id)->first();
        $l->reservation = $app->request->post('nom');
        $l->messrev = $app->request->post('message');
        $_SESSION['nomsave'] = $app->request->post('nom');

        $l->update();

        $app->redirect($app->urlFor('route_item',['id'=>$id , 'url'=>$_SESSION['liste']]));
    }

    static function rendrepublic(){
        $app =\Slim\Slim::getInstance();
        $id =  Liste :: where('no','=',$_SESSION['liste'])->first();
        $id->public = 1;
        $id->update();
        $app->redirect($app->urlFor('route_itemliste',['token'=>Liste::getToken($_SESSION['liste'])]));
    }

    static function rendreprivee(){
        $app =\Slim\Slim::getInstance();
        $id =  Liste :: where('no','=',$_SESSION['liste'])->first();
        $id->public = 0;
        $id->update();
        $app->redirect($app->urlFor('route_itemliste',['token'=>Liste::getToken($_SESSION['liste'])]));
    }

    static function participCagnotte($id){
        $app =\Slim\Slim::getInstance();
        $cagnotte = new Cagnotte();
        $cagnotte->noItem = $id;
        $cagnotte->participant = $app->request->post('nom');
        $cagnotte->valeur = $app->request->post('valeur');

        $cagnotte->save();

        $app->redirect($app->urlFor('route_item',['url'=>Liste::getToken($_SESSION['liste']), 'id'=>$id]));
    }

    static function creatorpublic(){
        $listes = Liste::get();
        $listecrea = null;
        $b = false;
        foreach ($listes as $lis){
            if($lis->public == 1 and $b == false){
                $listecrea[] = $lis->user_id;
                $b = true;   }
            else if($lis->public == 1 and in_array($lis->user_id,$listecrea) == false)
                $listecrea[] = $lis->user_id;
        }

return $listecrea;

}
}
