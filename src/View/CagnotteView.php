<?php
namespace mywishlist\View;
use mywishlist\Models\Cagnotte;
use mywishlist\Models\Item;
use mywishlist\Models\User;
use mywishlist\Controller\Connect;


class CagnotteView {

    private $item;

    public function __construct($id){
        $this->item = $id;
    }

    public function render() {
        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();

        $a = "";
        if(isset($_SESSION['id']))
            $a = User::getNom($_SESSION['id']);
        else
            if(isset($_SESSION['nomsave']))
                $a = $_SESSION['nomsave'];

        $app = \Slim\Slim::getInstance();
        $url2 = $app->urlFor('participCagnotte', ['id'=>$this->item]);

        //$html = $html . $this->item;

        $p = Cagnotte::getCagnotte($this->item);
        $item = Item::where('id','=',$this->item)->first();
        $max = $item->tarif - $p;

        $html = $html . '
            <body>
            <form method="post" action="'.$url2.'">
            <div class="row">
                <form class="col s12">
                  <div class="row">
                      <div class="input-field col s12">
                      <input name="nom" id="nom" value="'.$a.'" type="text" class="validate">
                      <label for="nom" class="active">Votre nom</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12">
                      <input name="valeur" id="valeur" type="number" value ="1" min="1" max="'.$max.'" class="validate">
                      <label for="valeur" class="active">Votre participation</label>
                    </div>
                  </div>
                  <div class="row">
                    <button class="col offset-l4 s4 m4 l4 btn waves-effect waves-light" type="submit"> Valider </button>
                  </div>
                </form>
              </div>
            </form>
            </body><html>';

        $html = $html.'</body><html>';

        return Connect::getHeader() . $html;
    }

}
