<?php
/**
 * Created by PhpStorm.
 * User: alex_
 * Date: 12/01/2018
 * Time: 20:53
 */

namespace mywishlist\View;
use mywishlist\Controller\Connect;
use mywishlist\Models\Liste;

class listepublicView
{

    public function render()
    {
        $app = \Slim\Slim::getInstance();
        $date = date('Y-m-d');
        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();
        $html = $html . '
        <table class="centered">
            <thead>
            <tr>
              <th>Nom</th>
             
            </tr>
            </thead>
        <tbody>';
        $listes = Liste::get();
        if (!$listes == null )
            foreach ($listes as $liste) {
            if($liste->public==1 and $liste->expiration >= $date){
                $url = $app->urlFor('route_itemliste', ['token' => $liste->token]);
                $html = $html . '
            <tr>
            <td><a href=' . $url . '>' . $liste->titre . '</a></td>
            
          </tr>
          ';}}
        return Connect::getHeader() . $html . '</body><html>';
    }
}