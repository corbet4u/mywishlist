<?php

namespace mywishlist\View;

use mywishlist\Controller\Connect;
use mywishlist\Models\Liste;

class ListView
{

    public function render()
    {
        $app = \Slim\Slim::getInstance();

        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();
        if(isset($_SESSION['id']))

        $html = $html . '
        <h1>Liste en cours</h1>
        <table class="centered">
            <thead>
            <tr>
              <th>Nom</th>
              <th>Description</th>
              <th>Date limite</th>
            </tr>
            </thead>
        <tbody>';
        $listes = Liste::getTest($_SESSION['id'], 1);
        if (!$listes == null)
            foreach ($listes as $liste) {
                $url = $app->urlFor('route_itemliste', ['token' => $liste->token]);
                $html = $html . '
            <tr>
            <td><a href=' . $url . '>' . $liste->titre . '</a></td>
            <td>' . $liste->description . '</td>
            <td>' . $liste->expiration . '</td>
          </tr>
          ';
        }$html = $html . '</tbody></table>';
        $html = $html . '
             <div class="row">
             <a href="' . $app->urlFor('newliste') . '" class="right btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>
             </div>';
        $html = $html . '<h1>Liste expiré</h1>
        <table class="centered">
            <thead>
            <tr>
              <th>Nom</th>
              <th>Description</th>
              <th>Date limite</th>
            </tr>
            </thead>
        <tbody>';
        $listes = Liste::getTest($_SESSION['id'], 0);
        if (!$listes == null)
            foreach ($listes as $liste) {
                $url = $app->urlFor('route_itemliste', ['token' => $liste->token]);
                $html = $html . '
            <tr>
            <td><a href=' . $url . '>' . $liste->titre . '</a></td>
            <td>' . $liste->description . '</td>
            <td>' . $liste->expiration . '</td>
          </tr>
          ';}$html = $html . '</tbody></table>';

           return Connect::getHeader() . $html . '</body><html>';
    }

}
