<?php
namespace mywishlist\View;
use mywishlist\Models\Cagnotte;
use mywishlist\Models\Item;
use mywishlist\Models\Liste;
use mywishlist\Controller\Connect;


class ItemView {

private $item;

public function __construct($i){
    $this->item=Item::getItem($i);
}

public function render() {
    $html = Connect::getDebutNav();
    $html = $html . Connect::getFinNav();
    $html = $html . '<body>';

    $t = '/';
    $t2 = $this->item->img;
    $t3 = strpos($t2, $t);
    if($this->item->cagnotte=='on')
        $tmp = Cagnotte::getCagnotte($this->item->id);
    else
        $tmp = -1;
    $html = $html . '<div class=centered>';
    if($this->item->img !=null)
    if($t3 == false)
        $html = $html.'<img class="center-align" src="../../img/'.$this->item->img.'" width="200">';
    else
        $html = $html.'<img class="center-align" src="'.$this->item->img.'" width="200">';

    $html = $html.'<h1 class="center-align">';
    $html = $html.$this->item->nom . '</h1>';

    if($tmp != -1)
        $html = $html . '<h1>Cagnotte : ' . $tmp . '/' . $this->item->tarif .'</h1>';

    if($this->item->url != null)
        $html = $html.'<a href="'.$this->item->url.'" class="center-align">Lien commercial</a>';

    $app = \Slim\Slim::getInstance();
    $date = date('Y-m-d');

    if(isset($_SESSION['id'])) {
        if ($_SESSION['id'] == Liste::getProprio($this->item->getListe()) and Liste::where('no', '=', $this->item->getListe())->first()->expiration >= $date) {
            if ($this->item->reservation == null) {
                if ($this->item->cagnotte == 'off') {
                    $html = $html . '
                    <div class="row"><a class="col s2 offset-s1 waves-effect waves-light btn"" href="' . $app->urlFor('suppritem', array('id' => $this->item->id)) . '">Supprimer item</a>';
                    $html = $html . '<a class="col s2 offset-s1 waves-effect waves-light btn"" href="' . $app->urlFor('modificationItem') . '">Modifier item</a></div>';
                } else if (Cagnotte::getCagnotte($this->item->id) == 0) {
                    $html = $html . '<div class="row"><a class="col s2 offset-s1 waves-effect waves-light btn" href="' . $app->urlFor('suppritem', array('id' => $this->item->id)) . '">Supprimer item</a>';
                    $html = $html . '<a class="col s2 offset-s1 waves-effect waves-light btn" href="' . $app->urlFor('modificationItem') . '">Modifier item</a></div>';
                }
            }
        }
    }

    if(isset($_SESSION['id'])) {
        if(Liste::getProprio(Liste::where('no','=',$this->item->getListe())->first()->user_id) != $_SESSION['id'] or Liste::where('no','=',$this->item->getListe())->first()->expiration < $date) {
            $p = Cagnotte::getParticipant($this->item->id);
            if ($p != null) {
                $html = $html . '<p>Liste des participants :';
                foreach ($p as $part) {
                    $html = $html . $part . '<br>';
                }
                $html = $html . '</p>';
            }
    }}else{
        if(isset($_COOKIE['proprio'])) {
            if($_COOKIE['proprio'] != Liste::getProprio(Liste::where('no','=',$this->item->getListe())->first()->user_id) or Liste::where('no','=',$this->item->getListe())->first()->expiration < $date){
                $p = Cagnotte::getParticipant($this->item->id);
                if ($p != null) {
                    $html = $html . '<p>Liste des participants :';
                    foreach ($p as $part) {
                        $html = $html . $part . '<br>';
                    }
                    $html = $html . '</p>';
                }
            }
        }else{
            $p = Cagnotte::getParticipant($this->item->id);
            if ($p != null) {
                $html = $html . '<p>Liste des participants :';
                foreach ($p as $part) {
                    $html = $html . $part . '<br>';
                }
                $html = $html . '</p>';
            }
        }
    }



    if($this->item->reservation==null and Liste::where('no','=',$this->item->getListe())->first()->expiration >= $date){
        if(isset($_COOKIE['proprio'])) {
            if ($_COOKIE['proprio'] != Liste::getProprio($this->item->liste_id)) {
                if ($this->item->cagnotte == 'off')
                    $html = $html . '<a class="waves-effect waves-light btn" href="' . $app->urlFor('reservation', ['url' => Liste::getToken($_SESSION['liste']), 'id' => $this->item->id]) . '">Réserver item</a>';
                else {
                    if($tmp < $this->item->tarif)
                        $html = $html . '<a class="waves-effect waves-light btn" href="' . $app->urlFor('Cagnotte', ['url' => Liste::getToken($_SESSION['liste']), 'id' => $this->item->id]) . '">Participer Cagnotte</a>';
                }
            }
        }else
            $html = $html . '<a class="waves-effect waves-light btn" href="' . $app->urlFor('reservation', ['url' => Liste::getToken($_SESSION['liste']), 'id' => $this->item->id]) . '">Réserver item</a>';

    }
    else{
        if(isset($_COOKIE['proprio'])) {
            if ($_COOKIE['proprio'] != Liste::getProprio($this->item->liste_id) or Liste::where('no','=',$this->item->getListe())->first()->expiration < $date) {
                if($_COOKIE['proprio'] == Liste::getProprio($this->item->liste_id)) {
                    $html = $html . '<p>Article réservé par ' . $this->item->reservation . '</p>';
                    $html = $html . '<p>Message laissé : ' . $this->item->messrev . '</p>';
                }else
                    $html = $html . '<p>Article réservé par ' . $this->item->reservation . '</p>';
            }else
                $html = $html . '<p>Article réservé</p>';
        }else
            $html = $html . '<p>Article réservé par ' . $this->item->reservation . '</p>';
    }

    $html = $html . '</div>';
    $html = $html.'</body><html>';

return Connect::getHeader() . $html;
}

}
