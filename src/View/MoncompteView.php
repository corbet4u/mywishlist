<?php


namespace mywishlist\View;
use mywishlist\Controller\Connect;

class MoncompteView
{



    public function render()
    {
        $app = \Slim\Slim::getInstance();
        $url1 = $app->urlFor('modificationInfo', []);
        $url2 = $app->urlFor('modificationPassword', []);
        $url3 = $app->urlFor('suppression', []);



        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();
        $html = $html . <<<END
<body xmlns="http://www.w3.org/1999/html">
 <div class="row">
  <br><br> <a href="$url1" class="col offset-l4 s4 m4 l4 waves-light waves-effect btn">Mofication des informations du compte</a>
  <br><br> <a href="$url2" class="col offset-l4 s4 m4 l4 waves-light waves-effect btn">Mofication du mot de passe</a>
   <br><br><a href="$url3" class=" red darken-1 col offset-l4 s4 m4 l4 waves-light waves-effect btn">Suppression du compte</a>
    
    </div> </body><html> 
END;

        return Connect::getHeader() . $html;


    }
}