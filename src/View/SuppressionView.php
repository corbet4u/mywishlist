<?php
/**
 * Created by PhpStorm.
 * User: alex_
 * Date: 10/01/2018
 * Time: 18:49
 */

namespace mywishlist\View;

use mywishlist\Controller\Connect;

class SuppressionView
{


    public function render()
    {
        $app = \Slim\Slim::getInstance();

        $url1 = $app->urlFor('moncompte',[]);
        $url2 = $app->urlFor('supprimercompte');
        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();
        $html = $html . <<<END
<body xmlns="http://www.w3.org/1999/html">
<h1>Êtes vous sûr ?</h1>
  <a href="$url2" class=" red darken-1 waves-light waves-effect btn">Oui</a>
   <a href="$url1" class=" green darken-1waves-light waves-effect btn">Non</a>
   
     </body><html> 
END;

        return Connect::getHeader() . $html;


    }
}