<?php

namespace mywishlist\View;

use mywishlist\Controller\Connect;


class InscriptionViewer
{


    public function __construct()
    {
    }

    public function render()
    {

        $app = \Slim\Slim::getInstance();
        $url = $app->urlFor('route_home', []);
        $url2 = $app->urlFor('inscrit', []);

        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();

        $html = $html . <<<END
<body>

<form method="post" action="$url2">
<div class="row">
    <form class="col s12">
    <div class="row">
          <div class="input-field col s12">
          <input required name='login' id="login" type="text" class="validate">
          <label for="login">Nom d'utilisateur</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s6">
          <input required name='nom' placeholder="Placeholder" id="first_name" type="text" class="validate">
          <label for="first_name">Nom</label>
        </div>
        <div class="input-field col s6">
          <input required name='prenom' id="last_name" type="text" class="validate">
          <label for="last_name">Prenom</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input required name='password' id="password" type="password" class="validate">
          <label for="password">Mot de passe</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input required name='email' id="email" type="email" class="validate">
          <label for="email">Email</label>
        </div>
      </div>
      <div class="row">
        <input class="col offset-l4 s4 m4 l4 btn waves-effect waves-light" type="submit" value="Valider" />
      </div>
    </form>
  </div>

</form>
</body><html>

END;
        return Connect::getHeader() . $html;
    }

}
