<?php
namespace mywishlist\View;
use mywishlist\Controller\Connect;
use mywishlist\Models\User;


class ReservationItem{

    public function render($id) {
        $app =\Slim\Slim::getInstance();

        $url2 = $app->urlFor('reserv', ['id'=>$id] );
        $a = "";
        if(isset($_SESSION['id']))
            $a = User::getNom($_SESSION['id']);
        else
            if(isset($_SESSION['nomsave']))
                $a = $_SESSION['nomsave'];

        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();
        $html = $html . <<<END
<body>
<form method="post" action="$url2">
<div class="row">
    <form class="col s12">
      <div class="row">
          <div class="input-field col s12">
          <input name='nom' id="nom" value="$a" type="text" class="validate">
          <label for="nom">Votre nom</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input name='message' id="message" type="text" class="validate">
          <label for="message">Votre message ( optionnel )</label>
        </div>
      </div>
      <div class="row">
        <button class="col offset-l4 s4 m4 l4 btn waves-effect waves-light" type="submit"> Valider </button>
      </div>
    </form>
  </div>
</form>
</body><html>

END;
        return Connect::getHeader() . $html;
    }

}
