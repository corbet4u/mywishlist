<?php
namespace mywishlist\View;
use mywishlist\Models\Cagnotte;
use mywishlist\Models\Item;
use mywishlist\Controller\Connect;
use mywishlist\Models\Liste;


class ItemListView{

    private $num;

    public function __construct($num){
        $this->num = $num;
    }

    public function render() {
        $app =\Slim\Slim::getInstance();
        $date = date('Y-m-d');
        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();
        $html = $html . '<body>
            <table class="centered">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Descr</th>
                        <th>Tarif</th>
                        <th>Etat</th>
                    </tr>
                </thead>
            <tbody>';

        $items = Item::getTest($_SESSION['liste']);
        if(!$items==null)
            foreach ($items as $item) {
                $url = $app->urlFor('route_item', ['url' => $this->num, 'id' => $item->id]);
                $html = $html . '
                <tr>
                <td><a href=' . $url . '>' . $item->nom . '</a></td>
                <td>' . $item->descr . '</td>
                <td>' . $item->tarif . '</td>';
                $p = Cagnotte::getCagnotte($item->id);
                if ($item->cagnotte == 'off') {
                    if ($item->reservation == null)
                        $html = $html . '<td>non réservé</td></tr>';
                    else
                        $html = $html . '<td>réservé</td></tr>';
                } else {
                    $html = $html . '<td>[' . $p . '/' . $item->tarif . ']</td></tr>';
                }
            }
            if(isset($_SESSION['id']))
                if(Liste::where('token','=',$this->num)->first()->user_id == $_SESSION['id'] and Liste::where('token','=',$this->num)->first()->expiration >= $date ){
                    $html = $html . '<br>
                    <div class="fixed-action-btn">
                    <a href="' . $app->urlFor('newitem') . '" class=" btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a></div>';
                    $html = $html . '
                   <div class="row">
                    <a class="col s2 offset-s1 waves-effect waves-light btn" href=' . $app->urlFor('supprliste') . '>Supprimer liste</a>';
                    $html = $html . '<a class="col s2 offset-s1 waves-effect waves-light btn" href=' . $app->urlFor('modificationListe') . '>Modifier liste</a>';
                    $l =Liste::where('token','=',$this->num)->first();
                    if($l->public == 1)
                        $html = $html . '<a class="col s2 offset-s1 waves-effect waves-light btn" href=' . $app->urlFor('rendreprivee') . '>Rendre la liste privée</a></div>';
                    else
                    $html = $html . '<a class="col s2 offset-s1 waves-effect waves-light btn" href=' . $app->urlFor('rendrepublic') . '>Rendre la liste public</a></div>';
                }
        return Connect::getHeader() .$html . '</body><html>';
    }
}