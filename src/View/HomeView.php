<?php

namespace mywishlist\View;

use mywishlist\Controller\Connect;

/**
 * Created by PhpStorm.
 * User: PoweR
 * Date: 18/12/2017
 * Time: 10:53
 */
class HomeView
{


    public function __construct()
    {
    }

    public function render()
    {
        $app = \Slim\Slim::getInstance();
        $url7 = $app->urlFor('listepublic', []);
        $url8=$app->urlFor('createurliste',[]);
        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();
        $html = $html . '<body background=img/gift.jpg>
        <div class="row">
        <br><br><br><br><br>    <a class="col offset-l4 s4 m4 l4 waves-effect waves-light btn" href=' . $url8 . '>Voir les créateurs de liste</a> 
         <br><br>   <a class="col offset-l4 s4 m4 l4 waves-effect waves-light btn" href=' . $url7 . '>Voir les listes publiques</a> 
        </div>
</body>';
        $html = $html . '</body><html>';


        return Connect::getHeader() . $html;
    }
}

