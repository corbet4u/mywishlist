<?php
/**
 * Created by PhpStorm.
 * User: alex_
 * Date: 16/01/2018
 * Time: 18:50
 */

namespace mywishlist\View;


use mywishlist\Controller\Connect;
use mywishlist\Models\User;

class CreatorView
{
    public function render()
    {
        $app = \Slim\Slim::getInstance();

        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();
        $html = $html . '
        <h1>Liste des créateurs</h1>
        <table class="centered">
            <thead>
            <tr>
              <th>Prenom</th>
              <th>Nom</th>
            </tr>
            </thead>
        <tbody>';
        $listes = Connect::creatorpublic();
        if (!$listes == null)
            foreach ($listes as $num) {
                $u = User::where('id','=',$num)->first();
                $html = $html . '
            <tr>
            <td>' . $u->prenom . '</td>
            <td>' . $u->nom . '</td>
          </tr>
          ';
            }$html = $html . '</tbody></table>';

         return Connect::getHeader() . $html . '</body><html>';
    }
}