<?php
namespace mywishlist\View;
use mywishlist\Controller\Connect;


class NewListView{

    public function render() {
        $app =\Slim\Slim::getInstance();

        $url2 = $app->urlFor('createlist', [] );
        $date = date("Y-m-d");

        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();
        $html = $html . <<<END
<body>
<form method="post" action="$url2">
<div class="row">
    <form class="col s12">
      <div class="row">
          <div class="input-field col s12">
          <input required name='nom' id="nom" type="text" class="validate">
          <label for="nom">Nom de la liste</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input name='descr' id="descr" type="text" class="validate">
          <label for="descr">Description</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input name='date' min="$date" value="$date" id="date" type="date" class="datepicker">
          <label class="active" for="date">Date d'expiration</label>
        </div>
      </div>
      <div class="row">
        <button class="col offset-l4 s4 m4 l4 btn waves-effect waves-light" type="submit"> Valider </button>
      </div>
    </form>
  </div>
</form>
</body><html>

END;
        return Connect::getHeader() . $html;
    }

}
