<?php
namespace mywishlist\View;
use mywishlist\Controller\Connect;

class NewItemView{

    public function render() {
        $app =\Slim\Slim::getInstance();

        $url2 = $app->urlFor('createitem', [] );

        $html = Connect::getDebutNav();
        $html = $html . Connect::getFinNav();
        $html = $html . <<<END
<body>
<form method="post" action="$url2">

    <div class="row">
        <form class="col s12">
          <div class="row">
              <div class="input-field col s12">
              <input required name='nom' id="nom" type="text" class="validate">
              <label for="nom">Nom</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input name='descr' id="descr" type="text" class="validate">
              <label for="descr">Description</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input required name='tarif' id="tarif" type="number" class="validate">
              <label for="tarif">Tarif</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input name='image' id="image" type="text" class="validate">
              <label for="image">Lien image</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input name='vente' id="vente" type="text" class="validate">
              <label for="vente">Lien commercial</label>
            </div>
          </div>
          <div class="row">
            <div class="switch">
                <label>
                    Réservation Direct
                    <input type="checkbox" id="cagnotte" name="cagnotte">
                    <span class="lever"></span>
                    Cagnotte
                </label>
            </div>
          </div>
          <div class="row">
            <button class="col offset-l4 s4 m4 l4 btn waves-effect waves-light" type="submit"> Valider </button>
          </div>
        </form>
      </div>


</form>
</body><html>

END;
        return Connect::getHeader() . $html;
    }

}
