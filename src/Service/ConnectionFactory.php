<?php
namespace mywishlist\Service;

use Illuminate\Database\Capsule\Manager as DB;
/**
 * Created by PhpStorm.
 * User: PoweR
 * Date: 14/11/2017
 * Time: 10:41
*/
class ConnectionFactory
{
    static $ini_array,$dsn;
    static function setConfig($file){
        ConnectionFactory::$ini_array = parse_ini_file($file);
    }
    static function connectORM(){
        $db = new DB();
        $conf = parse_ini_file('config.ini');
        $db->addConnection($conf);
        $db->setAsGlobal();
        $db->bootEloquent();
    }
}