<?php
/**
 * Created by PhpStorm.
 * User: PoweR
 * Date: 04/12/2017
 * Time: 11:04
 */

require_once __DIR__ . '/vendor/autoload.php';

use mywishlist\Controller\Controller;
use mywishlist\Service\ConnectionFactory;
use mywishlist\Controller\Connect;

session_start();
$app = new \Slim\Slim();

$app->get('/',function () {
    $g = new Controller();
    $g->showHome();
})->name('route_home');

$app->get('/liste', function(){
    $Il = new Controller();
    $Il->showList();
})->name('route_liste');

$app->get('/liste/:token/items', function($token){
    $Il = new Controller();
    $Il->showItemList($token);
})->name('route_itemliste');

$app->get('/:url/items/:id', function($url , $id){
    $I = new Controller();
    $I->showItem($id);
})->name('route_item');

$app->get('/inscription', function(){
    $I = new Controller();
    $I->showInscription();
})->name('inscription');

$app->get('/connexion', function(){
    $I = new Controller();
    $I->showConnection();
})->name('connexion');

$app->post('/connect', function(){
    Connect::connection();
})->name('connect');

$app->post('/inscription', function(){
    Connect::inscrire();
})->name('inscrit');

$app->post('/createliste', function(){
    Connect::newliste();
})->name('createlist');

$app->post('/createitem', function(){
    Connect::newitem();
})->name('createitem');

$app->get('/newliste', function(){
    $l = new Controller();
    $l -> showNewList();
})->name('newliste');

$app->get('/newitem', function(){
    $l = new Controller();
    $l -> showNewItem();
})->name('newitem');

$app->get('/deconnect', function(){
    Connect::disconnect();
})->name('deconnect');

$app->get('/supprliste', function(){
    Connect::supprliste();
})->name('supprliste');

$app->get('/suppritem/:id', function($id){
    Connect::suppritem($id);
})->name('suppritem');

$app->get('/moncompte',function(){
    $i=new Controller();
    $i->showMonCompte();
})->name('moncompte');

$app->get('/moncompte/modificationInfo',function (){
    $i=new Controller();
    $i->showModifInfo();
})->name('modificationInfo');

$app->get('/moncompte/modificationPassword',function (){
    $i=new Controller();
    $i->showModifPass();
})->name('modificationPassword');

$app->post('/connexion',function(){
    Connect::modifmdp();
})->name('modifmdp');

$app->post('/modifinfo',function (){
    Connect::modifinfo();
})->name('modifinfo');

$app->post('/modifListe',function (){
    Connect::modifListe();
})->name('modifListe');

$app->post('/modifItem/:id',function ($id){
    Connect::modifItem($id);
})->name('modifItem');

$app->get('/modificationItem',function (){
    $i=new Controller();
    $i->showModifItem();
})->name('modificationItem');

$app->get('/modificationListe',function (){
    $i=new Controller();
    $i->showModifListe();
})->name('modificationListe');

$app->get('/moncompte/suppression',function (){
    Controller::showSuppression();
})->name('suppression');

$app->get('/moncompte/supprimercompte',function (){
    Connect::suppricompte();
})->name('supprimercompte');

$app->get('/:url/:id/reservation',function ($url,$id){
    $i=new Controller();
    $i->showReservItem($id);
})->name('reservation');

$app->post('/reserv/:id',function ($id){
    Connect::reserverItem($id);
})->name('reserv');

$app->get('/rendrepublic',function (){
    Connect::rendrepublic();
})->name('rendrepublic');

$app->get('/rendreprivee',function (){
    Connect::rendreprivee();
})->name('rendreprivee');

$app->get('/listepublic',function(){
    Controller::showListepublic();
})->name('listepublic');

$app->post('/participCagnotte/:id',function($id){
    Connect::participCagnotte($id);
})->name('participCagnotte');

$app->get('/:url/Cagnotte/:id',function($url,$id){
    $c = new Controller();
    $c->showCagnotte($id);
})->name('Cagnotte');

$app->get('/Createur',function (){
    Controller::showCreator();
})->name('createurliste');

ConnectionFactory::connectORM();
$app->run();